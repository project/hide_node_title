## Hide Node Title

This module hides the title for content types and content types using 
display suite.

Instructions for use:

Pre-requisite, if using nested fieldsets group using `htmlelement`

1. Add the field `field_hide_title` as a checkbox to your content type and
    place with the title.
2. on the content check the `field_hide_title` to hide the title
